#Extract ingredients from recipe1m dataset


import json
import time

big_list = []

with open('dataset/det_ingrs.json') as json_file:
	data = json.load(json_file)
	for blob in data:
		label = blob['valid']
		ingredients = blob['ingredients']
		ingredients = list(map(lambda x: x['text'], ingredients))
		ingredients = list(zip(label, ingredients))
		ingredients = list(filter(lambda x: x[0] == True, ingredients))
		ingredients = list(map(lambda x: x[1], ingredients))
		ingredients = list(map(lambda x: x.lower(), ingredients))
		ingredients = list(map(lambda x: x.replace(',', ''), ingredients))
		ingredients = list(filter(lambda x: x != '', ingredients))
		big_list.extend(ingredients)

ingredients = set(big_list)
print(ingredients)
time.sleep(200)
max_three_words_ingredients = [i for i in ingredients if len(i.split(' ')) <= 3]
max_three_words_ingredients.sort(key=lambda x: len(x.split(' ')), reverse=True)
max_two_words_ingredients = [i for i in ingredients if len(i.split(' ')) <= 2]
max_two_words_ingredients.sort(key=lambda x: len(x.split(' ')), reverse=True)


with open('ingredient_train_max_three.json', 'w') as outfile:
	json.dump(max_three_words_ingredients, outfile, indent=4)
 
with open('ingredient_train_max_two.json', 'w') as outfile:
	json.dump(max_two_words_ingredients, outfile, indent=4)