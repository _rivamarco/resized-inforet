#experiment

from inflection import singularize, pluralize

print(singularize('italian mushrooms'))
print(singularize('italian mushroom'))
print(pluralize('italian mushroom'))
print(pluralize('italian mushrooms'))
print(singularize('tablespoons'))