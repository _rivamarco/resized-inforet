# create training set for word2vec from mongodb

import json
import re
import spacy
import progressbar
from pymongo import MongoClient
import pymongo

all_r = []

host = 'localhost'
port = 27017

myclient = MongoClient(host, port)
db = myclient["recipesdb"]
recipes = db["new_recipes"]

pymongo_cursor = db.recipes.find().limit(1000000)

for recipe in pymongo_cursor:
	recipe_ingredients = []
	for recipe_ingredient in recipe['ingredients']:
		recipe_ingredients.append(recipe_ingredient['text'])
	all_r.append(recipe_ingredients)
nlp = spacy.load('models/80k_new_less/model-best')

all_recipes = []

for ingredients in all_r:
	recipe_ingredients = set()
	for ingredient in ingredients:
		ingredient = ingredient.lower()
		ingredient = re.sub(r"[^a-zA-Z0-9]+", ' ', ingredient).strip()
		ingredient = re.sub(r'\b\w{1,2}\b', ' ', ingredient).strip()
		ingredient = ' '.join(ingredient.split())
		if ingredient != '' or ingredient != ' ':
			recipe_ingredients.add(ingredient)
	all_recipes.append(recipe_ingredients)

print(len(all_recipes))

progressbar.printProgressBar(0, len(all_recipes), prefix = 'Progress:', suffix = 'Complete', length = 50)


train = []

for i, recipe in enumerate(all_recipes):
	train_ingredients = set()
	for ingredient in recipe:
		doc = nlp(ingredient)
		for ent in doc.ents:
			train_ingredients.add(ent.text)
	train.append(list(train_ingredients))
	progressbar.printProgressBar(i + 1, len(all_recipes), prefix = 'Progress:', suffix = 'Complete', length = 50)



with open('word2vec/train_word2vec_80k_three_1mln.json', 'w') as outfile:
	json.dump(train, outfile, indent=4)
