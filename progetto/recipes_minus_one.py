# experiment

import spacy
import re
import pymongo
from pymongo import MongoClient

host = 'localhost'
port = 27017

myclient = MongoClient(host, port)
db = myclient["recipesdb"]
recipes = db["new_recipes"]

pymongo_cursor = db.recipes.find().skip(600000).limit(4)

all_r = []

for recipe in pymongo_cursor:
	recipe_ingredients = []
	for recipe_ingredient in recipe['ingredients']:
		recipe_ingredients.append(recipe_ingredient['text'])
	all_r.append(recipe_ingredients)
nlp = spacy.load('models/60k_three/model-best')

all_recipes = []

for ingredients in all_r:
	recipe_ingredients = set()
	for ingredient in ingredients:
		ingredient = ingredient.lower()
		ingredient = re.sub(r"[^a-zA-Z0-9]+", ' ', ingredient).strip()
		ingredient = re.sub(r'\b\w{1,2}\b', ' ', ingredient).strip()
		ingredient = ' '.join(ingredient.split())
		if ingredient != '' or ingredient != ' ':
			recipe_ingredients.add(ingredient)
	all_recipes.append(recipe_ingredients)


predict = []

for i, recipe in enumerate(all_recipes):
	test_ingredients = set()
	for ingredient in recipe:
		doc = nlp(ingredient)
		for ent in doc.ents:
			test_ingredients.add(ent.text)
	predict.append(list(test_ingredients))
 
predict_minus_one = list(map(lambda x: x[:-1], predict))

print(predict_minus_one)
print(predict)