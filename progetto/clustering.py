# CLUSTERING

# Load w2v recipe model and perform clustering


from gensim.models import KeyedVectors
import json


model = KeyedVectors.load("1mln_80k_new_less.model")
w2v_vectors = model.wv.vectors
w2v_indices = {word: model.wv.vocab[word].index for word in model.wv.vocab}

vocab = list(model.wv.vocab)
print(len(vocab))
X = model[vocab]

print(model.wv.similar_by_word('eggs'))

from sklearn.manifold import TSNE
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 18})


tsne = TSNE(n_components=2)
X_tsne = tsne.fit_transform(X)





from sklearn.cluster import AffinityPropagation

optics = AffinityPropagation(damping=0.9)
cluster_labels = optics.fit_predict(X)

from collections import Counter

print(cluster_labels)
print(list(zip(Counter(cluster_labels).keys(), Counter(cluster_labels).values())))

clusters = dict(sorted(zip(vocab, map(lambda x: int(x), cluster_labels))))
sorted_clusters = sorted(zip(vocab, cluster_labels), key=lambda x: x[1])
print(sorted_clusters)

with open('clusters.json', 'w') as outfile:
    json.dump(clusters, outfile, indent=4)

import pandas as pd

df = pd.DataFrame(X_tsne, index=vocab, columns=['x', 'y'])

df['cluster'] = cluster_labels

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)


ax.scatter(df['x'], df['y'], c=cluster_labels)

for word, data in df.iterrows():
    ax.annotate(f"{word} - {data['cluster']}", (data['x'], data['y']))

plt.show()