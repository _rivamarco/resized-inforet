# Load recipe data in mongo, first raw data then cleaned data (new_recipes collection)


import pymongo
import json
from pymongo import MongoClient

host = 'localhost'
port = 27017

myclient = MongoClient(host, port)
db = myclient["recipesdb"]
recipes = db["recipes"]

with open('./dataset/recipe1M_layers/layer1.json') as file:
    data = json.load(file)
    
recipes.insert_many(data)

pymongo_cursor = db.recipes.find()

new_recipes = db['new_recipes']
data = []
for recipe in pymongo_cursor:
    ingredients = recipe['ingredients']
    title = recipe['title']
    id = recipe['id']
    ingredients = list(map(lambda x: x['text'], ingredients))
    r = {  '_id': id,
                'title': title,
                'ingredients': ingredients }
    data.append(r)
new_recipes.insert_many(data)